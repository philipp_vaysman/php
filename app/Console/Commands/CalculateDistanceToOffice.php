<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Proexe\BookingApp\Offices\Models\OfficeModel;
use App\Proexe\BookingApp\Utilities\DistanceCalculator;

//use Proexe\BookingApp\Utilities\DistanceCalculator;

class CalculateDistanceToOffice extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'bookingApp:calculateDistanceToOffice';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Calculates distance to office';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
	    $unit = 'km';
		$distanceCalculator = new DistanceCalculator($unit);
		$offices = OfficeModel::all()->toArray();

		$lat                = $this->ask( 'Enter Lat:', '14.12232322' );
		$lng                = $this->ask( 'Enter Lng:', '8.12232322' );

		$minDistance = PHP_FLOAT_MAX;
		$minDistanceOffice = $offices[0];

		foreach ($offices  as $office ) {

			$this->line( 'Distance to ' . $office['name']. ': ' . $distanceCalculator->calculate(
					[ $lat, $lng ],
					[ $office['lat'], $office['lng'] ])
			);
            $dist =  $distanceCalculator->findClosestOffice( [ $lat, $lng ], $office );
            if($dist < $minDistance){
                $minDistance = $dist;
                $minDistanceOffice = $office;
            }
        }
        $this->line( 'Closest office: ' . $minDistanceOffice['name'] ." ". $minDistance);
	}
}
