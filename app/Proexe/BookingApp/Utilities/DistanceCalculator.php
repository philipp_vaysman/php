<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace App\Proexe\BookingApp\Utilities;


class DistanceCalculator
{

    private $unit;

    public function __construct($unit)
    {
        $this->unit = strtoupper($unit);
    }

    /**
     * @param array $from
     * @param array $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate($from, $to)
    {

        if (!is_array($from) || !is_array($to)) {
            return -1;
        }
        if (count($from) !== 2 || count($to) !== 2) {
            return -1;
        }

        $distanceInMiles = $this->getDistanceInMiles($from[0], $from[1], $to[0], $to[1]);

        switch ($this->unit) {
            case 'KM':
                $distance = $distanceInMiles * 1.609344;
                break;
            case 'M':
                $distance = $distanceInMiles * 1.609344 * 1000;
                break;
            default:
                $distance = -1;
        }

        return $distance;
    }



    /*
     * CREATE PROCEDURE GET_NEAREST_OFFICE(lat FLOAT,lng FLOAT)
    * BEGIN
    *
    * SELECT offices.name, DEGREES(ACOS(sin(radians(lat)) * sin(radians(offices.lat)) +
    *                                   cos(radians(lat)) * cos(radians(offices.lat))*
    *                                   cos(radians(8-offices.lng))))*60*1.1515*1.609344 Distance
    * FROM offices
    * WHERE DEGREES(ACOS(sin(radians(lat)) * sin(radians(offices.lat)) +
    *                    cos(radians(lat)) * cos(radians(offices.lat))*
    *                    cos(radians(lng-offices.lng))))*60*1.1515*1.609344 =
    *       (SELECT MIN(DEGREES(ACOS(sin(radians(lat)) * sin(radians(offices.lat))+
    *                                cos(radians(lat)) * cos(radians(offices.lat))*
    *                                cos(radians(lng-offices.lng))))*60*1.1515*1.609344)
    *        FROM offices)
    * ;
    * END;

    * CALL  GET_NEAREST_OFFICE(12,9)
     *
     *
     */


    /**
     * @param float $lat1
     * @param float $lon1
     * @param float $lat2
     * @param float $lon2
     *
     * @return float
     */
    private function getDistanceInMiles(float $lat1, float $lon1, float $lat2, float $lon2): float
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;

            return $miles;
        }
    }

    /**
     * @param array $from
     * @param array $office
     *
     * @return array
     */
    public function findClosestOffice($from, $office)
    {
        return $this->calculate($from, [$office['lat'], $office['lng']]);
    }

}