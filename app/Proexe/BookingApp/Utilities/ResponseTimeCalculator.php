<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace App\Proexe\BookingApp\Utilities;


use App\Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {

    public function calculate($bookingDateTime, $responseDateTime, $officeHours)
    {

        try {
            $created = new \DateTime('@'.strtotime($bookingDateTime));
            $updated = new \DateTime('@'.strtotime($responseDateTime));
        } catch (\Exception $e) {
        }

        $daysPassed = $updated->diff($created)->format('%d');
        $countOfMinutes = 0;

        $currentDate = $bookingDateTime;
        while ($daysPassed != 0){
            $officeHoursOpenedFrom = $this->getOfficeHoursFrom($officeHours,$this->getDayOfWeek($currentDate));
            if($officeHoursOpenedFrom == -1){
                $daysPassed--;
                continue;
            }
            $stringOpened = $created->format('Y/m/d').' '.$officeHoursOpenedFrom;
            $openedDateTime = $this->getDateTimeFromString($stringOpened);
            $officeHoursOpenedTo = $this->getOfficeHoursTo($officeHours,$this->getDayOfWeek($currentDate));
            $stringClosed = $created->format('Y/m/d').' '.$officeHoursOpenedTo;
            $closedDateTime = $this->getDateTimeFromString($stringClosed);

//            var_dump($openedDateTime);
//            var_dump($closedDateTime);
//            return 1;

            $countOfMinutes+=intval($closedDateTime->diff($openedDateTime)->h*60)+intval($closedDateTime->diff($openedDateTime)->i);
            $currentDate = strtotime($bookingDateTime. ' + 1 days');
            $daysPassed--;
        }

        $stringOpened = $created->format('Y/m/d').' '.$this->getOfficeHoursFrom($officeHours,$this->getDayOfWeek($bookingDateTime));
        $openedTime = strtotime($stringOpened);
        $openedDateTime = new \DateTime('@'.$openedTime);

        echo $bookingDateTime. " || ".$responseDateTime. " passed : ".$updated->diff($openedDateTime)->h." h ".$updated->diff($openedDateTime)->i." m opened from : ".$openedDateTime->format('H:i').". days passed : ".$daysPassed."\n";

        return $updated->diff($openedDateTime)->h*60+$updated->diff($openedDateTime)->i + $countOfMinutes;

    }

    private function getDateTimeFromString($s){
        $t = strtotime($s);
        $dateTime = new \DateTime('@'.$t);
        return $dateTime;
    }

    private function getDayOfWeek($date ){
        return date('w',strtotime($date));
    }

    private function getOfficeHoursFrom($officeHoursJson,$dayOfWeek){
        if(!$officeHoursJson[$dayOfWeek]['isClosed'])
            return $officeHoursJson[$dayOfWeek]['from'];
        return -1;
    }


    private function getOfficeHoursTo($officeHoursJson,$dayOfWeek){
        if(!$officeHoursJson[$dayOfWeek]['isClosed'])
            return $officeHoursJson[$dayOfWeek]['to'];
        return -1;
    }
}