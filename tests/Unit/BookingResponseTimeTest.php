<?php

namespace Tests\Unit;

use App\Proexe\BookingApp\Utilities\ResponseTimeCalculator;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookingResponseTimeTest extends TestCase
{
    /** @test */
    public function testTask4()
    {
        $office_hours = [
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
            ['to'=>"16:00","from"=>"10:00","isClosed"=>false],
        ];
        $rtc = new ResponseTimeCalculator();
        $res = $rtc->calculate('2019-08-06 10:40:00','2019-08-06 10:45:00',$office_hours);
        $res2 = $rtc->calculate('2019-08-07 10:51:00','2019-08-06 11:00:00',$office_hours);
        $this->assertEquals('5',$res);
        $this->assertEquals('9',$res2);
    	//You may need:
	    //$this->assertEquals($expected, $actual);
	    //Or
//        $this->assertTrue(true);
    }
}
